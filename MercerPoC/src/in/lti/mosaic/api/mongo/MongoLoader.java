package in.lti.mosaic.api.mongo;

import org.apache.commons.lang3.StringUtils;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCollection;

import in.lnt.constants.Constants;
import in.lnt.utility.general.Cache;

/**
 * @author rushikesh
 *
 */
public class MongoLoader {

	private static final Logger logger = LoggerFactory.getLogger(MongoLoader.class);
	
	private static String requestCollectionName = Cache
			.getMLProperty(in.lnt.utility.constants.CacheConstats.MONGO_REQUEST_COLLECTION_NAME);
	private static String responseCollectionName = Cache
			.getMLProperty(in.lnt.utility.constants.CacheConstats.MONGO_RESPONSE_COLLECTION_NAME);
	public static ObjectMapper mapper = new ObjectMapper();

	/**
	 * 
	 * @param mongoObject
	 */
	public static void dumpDataToMongo(MongoObject mongoObject) {

		try {
			logger.debug(">> dumpDataToMongo ");

			String collectionName = null;
			if (StringUtils.equalsIgnoreCase(mongoObject.getObjectType(), Constants.REQUEST)) {
				collectionName = requestCollectionName;
			} else {
				collectionName = responseCollectionName;
			}
			MongoCollection<Document> collection = MongoConnector.getMongoDBConnection().getCollection(collectionName);

			Document doc = new Document();
			doc.put("request_id", mongoObject.getRequestId() + "");
			doc.put("json", mongoObject.getObjectJson());
			doc.put("timeStamp", new BasicDBObject("date", mongoObject.getTimeStamp()));
			/*org.json.JSONObject object = new org.json.JSONObject();
			Map<String, Object> map = new HashMap<String, Object>();
			object.put("request_id", mongoObject.getRequestId() + "");
			object.put("json", mongoObject.getObjectJson());
			BasicDBObject date= new BasicDBObject("date", mongoObject.getTimeStamp());
			object.put("timeStamp", date);*/

			// dbObject = (DBObject) JSON.parse(object + "");
			// doc.get("json");

			collection.insertOne(doc);

			logger.debug("<< dumpDataToMongo ");
		} catch (Exception e) {
			logger.error("Error in dumpDataToMongo : " + e.getMessage());
		}
	}

}
