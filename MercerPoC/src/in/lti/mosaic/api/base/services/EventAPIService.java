package in.lti.mosaic.api.base.services;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import in.lnt.utility.constants.LoggerConstants;
import in.lti.mosaic.api.base.configs.Cache;
import in.lti.mosaic.api.base.configs.CacheConstants;
import in.lti.mosaic.api.base.loggers.ParamUtils;

/**
 * 
 * @author Balkrushna Patil
 *
 * This Class is use for registering and adding event while processing async request
 */
public class EventAPIService {
	private static final Logger logger = LoggerFactory.getLogger(EventAPIService.class);
	
	/**
	 * 
	 * @param request_id
	 * @param status
	 * @return
	 * @throws IOException
	 * 
	 * 
	 * This method is use for adding event while processing request
	 */
	public static String addEvent(String request_id, String status) throws IOException{
		logger.debug(LoggerConstants.LOG_MAXIQAPI + ">> addEvent "+ParamUtils.getString(request_id, status));
		String eventApiKey = Cache.getProperty(CacheConstants.EVENT_API_KEY);
		String eventType = Cache.getProperty(CacheConstants.EVENT_TYPE);
		String data = "{ \"event\":{\"AppKey\":\""+eventApiKey+"\", \"Source\": [], \"EventType\":\""+eventType+"\", \"AppEventType\":\"\", \"EventBody\":{ \"request_id\":\""+request_id+"\",\"status\":\""+status+"\" }} }";
		OkHttpClient client = new OkHttpClient();
		String eventURL = Cache.getProperty(CacheConstants.EVENT_API_URL);

		MediaType mediaType = MediaType.parse("application/json");
		RequestBody body = RequestBody.create(mediaType, data);
		Request request = new Request.Builder().url(eventURL)
				.put(body).addHeader("apikey", eventApiKey)
				.addHeader("content-type", "application/json").addHeader("cache-control", "no-cache")
				.build();

		Response response = client.newCall(request).execute();
		
		String message = response.message();
		logger.debug(LoggerConstants.LOG_MAXIQAPI + "<< addEvent "+ParamUtils.getString(message));
		return message;
		
	} 
	
	public String registerEventType(String registerEventURL, String eventType, String request_id, String status) throws IOException {
		logger.debug(LoggerConstants.LOG_MAXIQAPI + ">> registerEventType "+ParamUtils.getString(registerEventURL, request_id, status));
		String data="{\"EventMaster\":{\"EventName\":"+eventType+",\"EventDesc\":\"This event is used for saving state of async progress\",\"EventStr\":{\"request_id\":"+request_id+", \"status\":"+status+"} } }";
		String eventApiKey = Cache.getProperty(CacheConstants.EVENT_API_KEY);
		OkHttpClient client = new OkHttpClient();

		MediaType mediaType = MediaType.parse("application/json");
		RequestBody body = RequestBody.create(mediaType, data);
		Request request = new Request.Builder()
		  .url(registerEventURL)
		  .post(body)
		  .addHeader("apikey", eventApiKey)
		  .addHeader("content-type", "application/json")
		  .addHeader("cache-control", "no-cache")
		  .build();

		Response response = client.newCall(request).execute();
		String message = response.message();
		logger.debug(LoggerConstants.LOG_MAXIQAPI + "<< registerEventType "+ParamUtils.getString(message));
		return message;
	}

}
