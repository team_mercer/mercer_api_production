package in.lnt.enums;

public enum ValidationTypes {

	required, oneOf, DataType, eligibility, Transformation, expression, dataTypeError, expressionError, eeIdAutoCorrect, 
	phone, email, range, rangeValidationRefTable, mlautocorrect, dropDown, sequentialCheck, predefinedValidations
}
