package in.lnt.utility.constants;

import java.lang.reflect.Field;

public interface MercerErrorConstants {

	String ERR_001 = "mosaic-error-message.csv_json_file_size";
	String ERR_002 = "mosaic-error-message.json_file_size";
	String ERR_003 = "mosaic-error-message.invalid_file_type";
	String ERR_004 = "mosaic-error-message.invalid_json";
	String ERR_005 = "mosaic-error-message.invalid_error_data";
	String ERR_006 = "mosaic-error-message.miltiple_values";
	String ERR_007 = "mosaic-error-message.variable_not_found";
	String ERR_008 = "mosaic-error-message.expression_error";
	String ERR_009 = "mosaic-error-message.argument_mismatch";
	String ERR_010 = "mosaic-error-message.incomplete_expression";
	String ERR_011 = "mosaic-error-message.params_mismatch";
	String ERR_012 = "mosaic-error-message.expression_syntax";
	String ERR_013 = "mosaic-error-message.params_type_mismatch";
	String ERR_014 = "mosaic-error-message.invalid_expression";
	String ERR_015 = "mosaic-error-message.invalid_params";
	String ERR_016 = "mosaic-error-message.no_function_in_db";
	String ERR_017 = "mosaic-error-message.invalid_aggregator_or_input";
	String ERR_018 = "mosaic-error-message.params_length";
	String ERR_019 = "mosaic-error-message.invalid_int";
	String ERR_020 = "mosaic-error-message.wrong_data_while_aggregation";
	String ERR_021 = "mosaic-error-message.runtime_error";
	String ERR_022 = "Mosaic-error-message.service_overloaded";
	String ERR_023 = "mosaic-error-message.duplicate_employeeid";
	String ERR_024 = "mosaic-error-message.min_value_error";
	String ERR_025 = "mosaic-error-message.min_value_alert";
	String ERR_026 = "mosaic-error-message.max_value_alert";
	String ERR_027 = "mosaic-error-message.max_value_error";
	String ERR_028 = "mosaic-error-message.invalid_type_for_error";
	//String ERR_029 = "mosaic-error-message.validation_type_required";
	String ERR_029 = "mosaic-error-message.field_required";
	String ERR_030 = "mosaic-error-message.invalid_file_type";
	String ERR_031 = "mosaic-error-message.invalid_json";
	String ERR_032 = "mosaic-error-message.csv_file_entity_number";
	String ERR_033 = "mosaic-error-message.entity_sheet_number_mismatch";
	String ERR_034 = "mosaic-error-message.repeated_entity";
	String ERR_035 = "mosaic-error-message.sheet_not_found";
	String ERR_036 = "mosaic-error-message.entitiy_sheet_sequence_mismatch";
	String ERR_037 = "mosaic-error-message.entitiy_ sheet_list_mismatch";
	String ERR_038 = "mosaic-error-message.employeeid_info_not_found";
	String ERR_039 = "mosaic-error-message.employeeid_missing";
	String ERR_040 = "mosaic-error-message.unexpected_error";
	String ERR_041 = "mosaic-error-message.required";
	String ERR_042 = "mosaic-error-message.not_in_enum";
	String ERR_043 = "mosaic-error-message.incorrect_format";
	String ERR_044 = "mosaic-error-message.incorrect_result";
	String ERR_045 = "mosaic-error-message.datatype_mismatch";
	String ERR_046 = "mosaic-error-message.Please provide values";
	String ERR_047 = "mosaic-error-message.message_from_request";
	String ERR_048 = "mosaic-error-message.multiple_values";
	String ERR_049 = "mosaic-error-message.incumbent_not_eligible";
	String ERR_050 = "mosaic-error-message.no_display_label";
	String ERR_051 = "mosaic-error-message.missing_table_name";
	String ERR_052 = "mosaic-error-message.missing_column";
	String ERR_053 = "mosaic-error-message.provide_one_of_values";
	String ERR_054 = "mosaic-error-message.sequential_check";
	String ERR_055 = "mosaic-error-message.invalid_excel_template";
	String ERR_056 = "mosaic-error-message.missing_mappedCompany";
	String ERR_057 = "mosaic-error-message.incumbent_not_eligible_error";
	String ERR_058 = "mosaic-error-message.incumbent_eligible_alert";
	String ERR_059 = "mosaic-error-message.system_autocorrect";
	String ERR_060 = "mosaic-error-message.file_acceptance";
	String ERR_061 = "mosaic-error-message.incorrect_integer_format";
	String ERR_062 = "mosaic-error-message.incorrect_double_format";
	String ERR_063 = "mosaic-error-message.incorrect_date_format";
	String ERR_064 = "mosaic-error-message.missing_header";
	String ERR_006_Params = "valuesQty:varName:expression:";
	String ERR_007_Params = "varName:expression";
	String ERR_033_Params = "entitiesNum:sheetsNum";
	String ERR_034_Params = "companyCode:countryCode";
	String ERR_035_Params = "sheetName";
	String ERR_040_Params = "details";
	String ERR_045_Params = "key:value:datatype";
	String ERR_041_Params = "key";
	String ERR_042_Params = "value:values";
	String ERR_043_Params = "value";
	String ERR_044_Params = "result";
	String ERR_048_Params = "valuesQty:varName:expression";
	String ERR_049_Params = "code:displayLabel";
	String ERR_050_Params = "code:displayLabel:code1:displayLabell";
	String ERR_052_Params = "tableName";
	String ERR_057_Params = "displayLabel:baseQuestionLabel";
	String ERR_058_Params = "displayLabel";
	String ERR_060_Params = "_id:code";

	public default String getError(String key) {

		String value = null;
		try {
			Field field = MercerErrorConstants.class.getField(key);
			value = (String) field.get(MercerErrorConstants.class);
		} catch (Exception e) {
			return null;
		}
		return value;
	}
}
