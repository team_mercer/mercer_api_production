package in.lnt.service.db;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.lti.mosaic.derby.MercerDBCP;
import com.lti.mosaic.function.def.CustomFunctions;
import in.lnt.constants.Constants;

public class DBUtils {

  private static final Logger logger = LoggerFactory.getLogger(DBUtils.class);

  private static Map<String, List<String>> refTableMetaDataCache =
      new HashMap<String, List<String>>();

  public static List<String> getRefTableMetaData(String tableName)
      throws SQLException, IOException {

    if (refTableMetaDataCache.containsKey(tableName)) {
      return refTableMetaDataCache.get(tableName);
    } else {
      List<String> list = new ArrayList<>();

      Connection connection = null;
      Statement stmt = null;
      ResultSet rs = null;

      ResultSetMetaData rsmetaData = null;
      int columnCount = 0;
      String query = "SELECT * FROM " + tableName + CustomFunctions.FIRST_ROW;

      long startTime = 0;
      try {
        startTime = System.currentTimeMillis();

        connection = MercerDBCP.getDataSource().getConnection();
        stmt = connection.createStatement();

        rs = stmt.executeQuery(query);
        rsmetaData = rs.getMetaData();

        columnCount = rsmetaData.getColumnCount();
        for (int i = 1; i <= columnCount; i++) {
          list.add(rsmetaData.getColumnName(i));
        }
        logger.debug(
            "Time taken for creating connection and getting result statement for getRefTableMetaData() "
                + "in DBUtiils..",
            (System.currentTimeMillis() - startTime));
      } catch (SQLException ex) {
        logger.error("Exception occured in getRefTableMetaData.." + ex.getMessage());
        throw ex;
      } finally {
        if (rs != null) {
          rs.close();
        }
        if (stmt != null) {
          stmt.close();
        }
        if (connection != null) {
          try {
            connection.close();
          } catch (Exception e) {
            logger.error("Error while closing connection {}", e);
          }
        }
      }
      refTableMetaDataCache.put(tableName, list);
      return list;
    }
  }


  private static Map<String, HashMap<String, String>> rangeDetailsCache =
      new HashMap<String, HashMap<String, String>>();


  public static HashMap<String, String> getRangeDetails(String query)
      throws SQLException, IOException {

    if (rangeDetailsCache.containsKey(query)) {
      return rangeDetailsCache.get(query);
    } else {


      HashMap<String, String> map = new HashMap<String, String>();

      Connection connection = null;
      Statement stmt = null;
      ResultSet rs = null;

      long startTime = 0;
      try {
        // conn = jdbcInstance.getConnection("jdbc:mysql://localhost:3306/", "mercerdb","root", "");

        startTime = System.currentTimeMillis();

        connection = MercerDBCP.getDataSource().getConnection();
        stmt = connection.createStatement();

        rs = stmt.executeQuery(query);
        while (rs.next()) {
          map.put(Constants.ERROR_MIN, rs.getString(Constants.ERROR_MIN));
          map.put(Constants.ERROR_MAX, rs.getString(Constants.ERROR_MAX));
          map.put(Constants.ALERT_MIN, rs.getString(Constants.ALERT_MIN));
          map.put(Constants.ALERT_MAX, rs.getString(Constants.ALERT_MAX));
        }
        logger.debug(
            "Time taken for creating connection and getting result statement for getRangeDetails() "
                + "in DBUtiils..",
            (System.currentTimeMillis() - startTime));
      } catch (SQLException ex) {
        map.put("SQLException", ex.getMessage());
        logger.error("Exception occured in getRangeDetails.." + ex.getMessage());
        throw ex;
      } finally {
        if (rs != null) {
          rs.close();
        }
        if (null != stmt) {
          stmt.close();
        }
        if (stmt != null) {
          stmt.close();
        }
        if (connection != null) {
          try {
            connection.close();
          } catch (Exception e) {
            logger.error("Error while closing connection {}", e);
          }
        }
      }
      rangeDetailsCache.put(query, map);
      return map;
    }
  }

  private static Map<String, Map<String, String>> refTableDataCache =
      new HashMap<String, Map<String, String>>();

  /**
   * @param tableName
   * @return Map with values from two columns from refrence table.
   * @throws SQLException
   * @throws IOException
   */
  public static Map<String, String> getRefTableData(String tableName)
      throws SQLException, IOException {
    if (refTableDataCache.containsKey(tableName)) {
      return refTableDataCache.get(tableName);
    } else {

      logger.info("Entering Inside  getRefTableData method");

      Map<String, String> dataMap = new HashMap<>();

      Connection connection = null;
      Statement stmt = null;
      ResultSet rs = null;

      final String query = " SELECT * FROM " + tableName;

      long startTime = 0;

      logger.info("query is :" + query);
      try {
        startTime = System.currentTimeMillis();

        connection = MercerDBCP.getDataSource().getConnection();
        stmt = connection.createStatement();


        rs = stmt.executeQuery(query);
        logger.info("rs is :" + rs.getMetaData());
        // Assuming table contains 2 columns only.
        while (rs.next()) {
          dataMap.put(rs.getString("CTRY_CODE"), rs.getString("CTRY_NAME"));
        }
        logger.debug(
            "Time taken for creating connection and getting result statement for getRefTableData() "
                + "in DBUtiils..",
            (System.currentTimeMillis() - startTime));
        logger.info("dataMap is :" + dataMap);
      } catch (SQLException ex) {
        logger.error("Exception occured in getRefTableData.." + ex.getMessage());
        throw ex;
      } finally {

        if (rs != null) {
          rs.close();
        }
        if (stmt != null) {
          stmt.close();
        }
        if (connection != null) {
          try {
            connection.close();
          } catch (Exception e) {
            logger.error("Error while closing connection {}", e);
          }
        }
      }

      logger.info("Existing from  getRefTableData method");
      refTableDataCache.put(tableName, dataMap);
      return dataMap;
    }
  }

  public static HashMap<String, ArrayList<HashMap<String, Object>>> loadDataBaseInMemory()
      throws SQLException, IOException {

    Connection connection = null;
    Statement stmt = null;
    ResultSet resultSet = null;

    long startTime = 0;

    HashMap<String, ArrayList<HashMap<String, Object>>> recordsMap = null;
    ArrayList<String> tableNames = null;
    String sql = null;
    ResultSetMetaData rsMd = null;
    int columnCount = 0;
    Object recrodObj = null;
    boolean isRecordExist = false;
    try {

      // Define linked hash map to store tables and its corr. columns.

      tableNames = new ArrayList<String>(50);
      recordsMap = new HashMap<String, ArrayList<HashMap<String, Object>>>();
      startTime = System.currentTimeMillis();
      logger.info(" DB Operation start time is " + startTime);
      connection = MercerDBCP.getDataSource().getConnection();
      // --- LISTING DATABASE TABLE NAMES ---
      tableNames = getAllTables();

      // --- LISTING DATABASE COLUMN NAMES AND RECORDS FROM EACH TABLE---
      stmt = connection.createStatement();
      for (String table : tableNames) {

        ArrayList<HashMap<String, Object>> tempArrayList = null;
        tempArrayList = new ArrayList<HashMap<String, Object>>();

        sql = " SELECT * FROM  " + table;
        resultSet = stmt.executeQuery(sql);
        rsMd = resultSet.getMetaData();
        columnCount = rsMd.getColumnCount();
        tempArrayList.clear();
        while (resultSet.next()) {
          HashMap<String, Object> tempMap = null;
          tempMap = new HashMap<String, Object>();

          isRecordExist = true;
          for (int i = 1; i <= columnCount; i++) {
            recrodObj = resultSet.getObject(i);
            tempMap.put(rsMd.getColumnName(i), recrodObj);
          }
          tempArrayList.add(tempMap);
        }
        if (isRecordExist) {
          recordsMap.put(table, tempArrayList);
        }
      }
      logger.info(" DB Operation end time is " + (System.currentTimeMillis() - startTime));

    } catch (SQLException ex) {
      logger.error("Exception occured in getRefTableData.." + ex.getMessage());
      throw ex;

    } finally {

      if (resultSet != null) {
        resultSet.close();
      }
      if (stmt != null) {
        stmt.close();
      }
      if (connection != null) {
        try {
          connection.close();
        } catch (Exception e) {
          logger.error("Error while closing connection {}", e);
        }
      }
    }
    // Files.write(Paths.get("C:\\Users\\10640514\\Desktop\\ABC.txt"),
    // recordsMap.toString().getBytes());
    return recordsMap;
  }

  public static ArrayList<String> getAllTables() throws SQLException {

    DatabaseMetaData dbMd = null;
    ResultSet resultSet = null;
    Connection connection = null;
    ArrayList<String> tableNames = new ArrayList<String>();
    // --- LISTING DATABASE TABLE NAMES ---
    try {
      // Take connection from the DB Connection pool.
      connection = MercerDBCP.getDataSource().getConnection();
      // Using sql connection get Meta Data for all tables in Database
      dbMd = connection.getMetaData();
      // Get tables listing in the resultset.
      resultSet = dbMd.getTables(null, null, "%", null);

      while (resultSet.next()) {
        resultSet.getString(2);
        tableNames.add(resultSet.getString(3));
      }
      logger.info("Table Name = " + tableNames);

    } catch (Exception ex) {
      logger.error("Exception occured in getTables.." + ex.getMessage());
      ex.printStackTrace();
    } finally {
      dbMd = null;
      if (resultSet != null) {
        resultSet.close();
      }
      if (connection != null) {
        try {
          connection.close();
        } catch (Exception e) {
          logger.error("Error while closing connection {}", e);
        }
      }
    }
    return tableNames;
  }

}
