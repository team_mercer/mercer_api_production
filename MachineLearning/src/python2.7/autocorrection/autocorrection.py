from flask import Flask, render_template, request
app = Flask(__name__)
import sys
reload(sys)
sys.setdefaultencoding('utf8')
import os
import json
import pandas
import math
import numpy as np
import multiprocessing
from multiprocessing import Pool
import re
from dateutil import parser
from datetime import datetime, timedelta
from pandas.io.json import json_normalize
pandas.options.mode.chained_assignment = None
from pandas.io import sql
import string
from fuzzywuzzy import fuzz
from flaskext.mysql import MySQL
from flask_cache import Cache   
from pathos.multiprocessing import ProcessingPool 	
import dill
import logging
logging.basicConfig(filename= 'flask.log',level=logging.DEBUG)
if not os.path.exists("./JsonLogging/"):
	os.makedirs("./JsonLogging/")


# define the cache config keys, remember that it can be done in a settings file
cache = Cache(app,config={'CACHE_TYPE': 'simple'})

api_home = os.getenv('API_HOME')
config_file = '{}/conf/config.properties'.format(api_home)
with open(config_file) as f:
    config = f.read().split('\n')
    for item in config:
        if item.startswith('MERCER_DB_ADDRESS'):
            MERCER_DB_ADDRESS = item
	if item.startswith('MERCER_DB_NAME'):
	    MERCER_DB_NAME = item
	if item.startswith('MERCER_DB_USERNAME'):
            MERCER_DB_USERNAME = item
	if item.startswith('MERCER_DB_PASSWORD'):
	    MERCER_DB_PASSWORD = item
        if item.startswith('MERCER_DB_HOST_PORT'):
            MERCER_DB_HOST_PORT = item



 
mysql = MySQL()
app = Flask(__name__)
app.config['MYSQL_DATABASE_USER'] = MERCER_DB_USERNAME.split("=")[-1].replace('\r','')
app.config['MYSQL_DATABASE_PASSWORD'] = MERCER_DB_PASSWORD.split("=")[-1].replace('\r','')
app.config['MYSQL_DATABASE_DB'] = MERCER_DB_NAME.split("=")[-1].replace('\r','')
app.config['MYSQL_DATABASE_HOST'] = MERCER_DB_ADDRESS.split("=")[-1].replace('\r','')
app.config['MYSQL_DATABASE_PORT'] = int(MERCER_DB_HOST_PORT.split("=")[-1].replace('\r',''))
mysql.init_app(app)




@cache.cached(timeout=60, key_prefix='Yes_No_Update')
def get_Yes_No_update():
	logging.debug('Yes no Cache Update')
	cursor = mysql.connect().cursor()
	cursor.execute("SELECT * from yes_no_lookup")
	Yes_No_Lookup = pandas.DataFrame(list(cursor.fetchall()), columns=map(lambda x:x[0], cursor.description))
	return Yes_No_Lookup

@cache.cached(timeout=60, key_prefix='Garbage_Update')	
def get_Garbage_update():
        logging.debug('Garbage Cache Update')
	cursor = mysql.connect().cursor()
	cursor.execute("SELECT * from garbage_lookup")
	garbage_lookup = pandas.DataFrame(list(cursor.fetchall()), columns=map(lambda x:x[0], cursor.description))
	return garbage_lookup


@app.route('/', methods=['GET', 'POST'])
def hello():
    return "AUTOCORRECTION OK"


@app.route('/upload', methods=['POST'])
def upload():
	try:
		if request.method == 'POST':
			a = datetime.now()
			import pdb
		#	pdb.set_trace()
                        Incoming_Json = request.get_json()
			Incoming_Json1=json.dumps(Incoming_Json)
			Incoming_Json1 = Incoming_Json1.replace('\n', '').replace('\r','').encode('utf-8')
			MJson_Raw = json.loads(Incoming_Json1)			

			SectionStructure = json_normalize(MJson_Raw)['sectionStructure.columns']
			SectionStructureList = filter(None, SectionStructure.tolist())
                        SectionStructure2 = json_normalize(SectionStructureList[0])

			def json_flatten(option):
    				try:
        				if type(option) is list:

             					return{'items' : option}
        				
					else:
            					return(option)
    				
				except Exception as e:
        				logging.debug('json_flatten failed')
					logging.debug(e)
        				return(option)


			if SectionStructure2.columns.tolist().count('options.items') > 0:
    				SectionStructure2['options.items'] = SectionStructure2.apply(lambda x:json_flatten(x['options.items']) , axis = 1)
    				SectionStructure2 = SectionStructure2.rename(columns = {'options.items':'options'})			
                        

			print(SectionStructure2.head())
			 
			Incoming_Data = MJson_Raw['data']
			Incoming_DataList = filter(None, Incoming_Data)
			Incoming_Data = json_normalize(Incoming_DataList)
			Incoming_Data = Incoming_Data.astype(str)
			Incoming_Data = Incoming_Data.replace('None','')
			Incoming_Data = Incoming_Data.replace('nan','')
                        logging.debug(str(datetime.now() -a ) + 'Incoming Json read')                        

		
			QuestionMaster = pandas.DataFrame(columns=['code', 'dataType', 'questionType'])
			QuestionMasterTemp = SectionStructure2[['code','dataType','questionType']]
			QuestionMaster = QuestionMaster.append(QuestionMasterTemp)
			QuestionMaster = QuestionMaster.dropna(axis=0, how='all')
		
			logging.debug(str(datetime.now() - a ) + 'Column categorization Checkpoint1')

		
                        if SectionStructure2.columns.tolist().count('options') > 0:
				DropDown1 = SectionStructure2[(SectionStructure2.questionType == 'dropdown') | (SectionStructure2.questionType == 'radio_buttons')][['code','options','dataType','questionType']]
			
			else:
				DropDown1 = SectionStructure2[['code','dataType','questionType']]
				DropDown1['options'] = 'nondd'


			DropDown1['DD_Status'] = DropDown1.apply(lambda x: DD_Status_Flag(x['options']), axis= 1)
			DropDownMaster = DD_Label_Value(DropDown1[DropDown1.options != 'nondd'])

			QuestionMaster['Flag1'] = QuestionMaster.apply(lambda x: Flag_1(x['code'],x['dataType'],x['questionType'], DropDownMaster),axis=1)  
			QuestionMaster['Flag2'] = QuestionMaster.apply(lambda x: Flag_2(x['code'],x['dataType'],x['questionType'],x['Flag1'], DropDownMaster),axis=1)
	
			logging.debug(str(datetime.now() - a ) + 'Column categorization Checkpoint2')

			DD_Large_Columns = sorted(QuestionMaster[QuestionMaster.Flag2 == 'DD_Large']['code'].unique().tolist())
			DD_Medium_Columns = sorted(QuestionMaster[QuestionMaster.Flag2 == 'DD_Medium']['code'].unique().tolist())
			DD_Small_Columns = sorted(QuestionMaster[QuestionMaster.Flag2 == 'DD_Small']['code'].unique().tolist())
			DD_Yes_No_Columns = sorted(QuestionMaster[QuestionMaster.Flag2 == 'DD_Yes_No']['code'].unique().tolist())
	
			
			DD_Large_Columns = list(set(DD_Large_Columns) & set(Incoming_Data.columns))
			DD_Medium_Columns = list(set(DD_Medium_Columns) & set(Incoming_Data.columns))
			DD_Small_Columns = list(set(DD_Small_Columns) & set(Incoming_Data.columns))
			DD_Yes_No_Columns = list(set(DD_Yes_No_Columns) & set(Incoming_Data.columns))
			logging.debug(str(datetime.now() -a ) + 'Column Categorization')
			
			Yes_No_Lookup = get_Yes_No_update()
			Yes_No_Dict = Yes_No_Lookup[['MATCH','REPLACEMENT']].set_index('MATCH')['REPLACEMENT'].to_dict()
			Garbage_Lookup = get_Garbage_update()
			Garbage_Dict = Garbage_Lookup[['MATCH','REPLACEMENT']].set_index('MATCH')['REPLACEMENT'].to_dict()
		


                        Incoming_Cols = Incoming_Data.columns
			Incoming_Cols_Flags = map(lambda x: x+'_ACflag', Incoming_Cols)
			Incoming_Cols_Replaces = map(lambda x: x+'_Replace', Incoming_Cols)
			DD_Large_Columns_Flags = map(lambda x: x+'_ACflag', DD_Large_Columns)
			DD_Large_Columns_Replaces = map(lambda x: x+'_Replace', DD_Large_Columns)
			DD_Medium_Columns_Flags = map(lambda x: x+'_ACflag', DD_Medium_Columns)
			DD_Medium_Columns_Replaces = map(lambda x: x+'_Replace', DD_Medium_Columns)
			DD_Small_Columns_Flags = map(lambda x: x+'_ACflag', DD_Small_Columns)
			DD_Small_Columns_Replaces = map(lambda x: x+'_Replace', DD_Small_Columns)		
			DD_Yes_No_Columns_Flags = map(lambda x: x+'_ACflag', DD_Yes_No_Columns)
			DD_Yes_No_Columns_Replaces = map(lambda x: x+'_Replace', DD_Yes_No_Columns)
			

                        All_DD_Columns = DD_Large_Columns + DD_Medium_Columns + DD_Small_Columns + DD_Yes_No_Columns
			All_DD_Columns_Flags = DD_Large_Columns_Flags + DD_Medium_Columns_Flags + DD_Small_Columns_Flags + DD_Yes_No_Columns_Flags
			All_DD_Columns_Replaces = DD_Large_Columns_Replaces + DD_Medium_Columns_Replaces + DD_Small_Columns_Replaces + DD_Yes_No_Columns_Replaces
     
                        ALL_Non_DD_Columns = list(set(Incoming_Cols) - set(All_DD_Columns))
			ALL_Non_DD_Columns_Flags = map(lambda x: x+'_ACflag', ALL_Non_DD_Columns)
			ALL_Non_DD_Columns_Replaces =  map(lambda x: x+'_Replace', ALL_Non_DD_Columns)

			
			#if len(DD_Large_Columns+DD_Medium_Columns+DD_Small_Columns+DD_Yes_No_Columns) < 1:
			#	raise Exception()
			
			for cols in DD_Large_Columns+DD_Medium_Columns+DD_Small_Columns+DD_Yes_No_Columns:
				Incoming_Data[cols] = Incoming_Data[cols].str.strip()
			
		        logging.debug(str(datetime.now() -a ) + 'Drop Down space stripper')

	
			Incoming_Data_Replace = Incoming_Data.copy()
			Incoming_Data_Replace.columns = map(lambda x: x+'_Replace',Incoming_Data_Replace.columns)
			Incoming_Data_ACflag = pandas.DataFrame('O',index = Incoming_Data.index, columns=map(lambda x: x+'_ACflag',Incoming_Cols))
			Incoming_Data3 = pandas.concat([Incoming_Data, Incoming_Data_Replace, Incoming_Data_ACflag], axis=1)
						
			
					
			def DD_MP(Incoming_Data2):
                                   
				Garbage_Processing_Df1 = Incoming_Data2[All_DD_Columns + All_DD_Columns_Flags + All_DD_Columns_Replaces]
				
				for col in All_DD_Columns:
                                	displayName_Temp = DropDownMaster[DropDownMaster.code == col]['displayName'].tolist()
					value_Temp = DropDownMaster[DropDownMaster.code == col]['value'].tolist()
					Garbage_Dict_Temp = {k : v for k,v in filter(lambda t: t[0] not in displayName_Temp + value_Temp , Garbage_Dict.iteritems())}
                                        Garbage_Processing_Df1[''.join([col,"_Replace"])] = Garbage_Processing_Df1[col].replace(Garbage_Dict_Temp)



				Garbage_Processing_Df2 = Incoming_Data2[ALL_Non_DD_Columns + ALL_Non_DD_Columns_Flags + ALL_Non_DD_Columns_Replaces]
				Garbage_Processing_Df2[ALL_Non_DD_Columns_Replaces] = Garbage_Processing_Df2[ALL_Non_DD_Columns].replace(Garbage_Dict)
				Incoming_Data2 = pandas.concat([Garbage_Processing_Df1, Garbage_Processing_Df2], axis=1)



				if len(DD_Yes_No_Columns) > 0:
					Processing_Df = Incoming_Data2[DD_Yes_No_Columns+DD_Yes_No_Columns_Flags+DD_Yes_No_Columns_Replaces]
					Non_Processing_Df = Incoming_Data2.drop(DD_Yes_No_Columns+DD_Yes_No_Columns_Flags+DD_Yes_No_Columns_Replaces, axis=1)
					Processing_Df[DD_Yes_No_Columns_Replaces] = Processing_Df[DD_Yes_No_Columns_Replaces].replace(Yes_No_Dict)
					Incoming_Data2 = pandas.concat([Processing_Df, Non_Processing_Df], axis=1)
				

					
					for col in DD_Yes_No_Columns:
						DD_Dict = DropDownMaster[DropDownMaster.code == col][['displayName','value']].set_index('displayName')['value'].to_dict()
						Incoming_Data2[''.join([col,"_Replace"])] = Incoming_Data2[''.join([col,"_Replace"])].replace(DD_Dict)
						Incoming_Data2[''.join([col,"_ACflag"])][Incoming_Data2[col] != Incoming_Data2[''.join([col,"_Replace"])]]  = 'R'
						Incoming_Data2[''.join([col,"_ACflag"])][((Incoming_Data2[''.join([col,"_Replace"])] == 'Y') | (Incoming_Data2[''.join([col,"_Replace"])] == 'N')) == False ] = 'X'
						Incoming_Data2[''.join([col,"_ACflag"])][((Incoming_Data2[''.join([col,"_ACflag"])] == 'X') & (Incoming_Data2[''.join([col])] == '')) == True ] = 'O'
	
                                        
  
                                        for col in DD_Yes_No_Columns:
						Ref_Data1 = str(DropDownMaster[DropDownMaster.code == col][['displayName','value']].set_index('displayName')['value'].to_dict())
						Incoming_Data2[''.join([col,"_Replace"])] = Incoming_Data2.apply(lambda x: Field_Resolver_Yes_No(x[''.join([col,"_Replace"])], Ref_Data1, x[''.join([col,"_ACflag"])] ), axis = 1)
						Incoming_Data2[''.join([col,"_ACflag"])] = Incoming_Data2.apply(lambda x: Layer_Flag_Update_V1(x[''.join([col,"_Replace"])],x[''.join([col,"_ACflag"])] ), axis = 1)
						Incoming_Data2[''.join([col,"_Replace"])] = Incoming_Data2.apply(lambda x: Field_Flag_Chopper(x[''.join([col,"_Replace"])]), axis = 1) 


     
				if len(DD_Small_Columns  + DD_Medium_Columns ) > 0:
				
					for col in DD_Small_Columns  + DD_Medium_Columns:
						Ref_Data1 = str(DropDownMaster[DropDownMaster.code == col][['displayName','value']].set_index('displayName')['value'].to_dict())
						Incoming_Data2[''.join([col,"_Replace"])] = Incoming_Data2.apply(lambda x: Field_Resolver_Dict_Based(x[''.join([col,"_Replace"])], Ref_Data1 ), axis = 1)
						Incoming_Data2[''.join([col,"_ACflag"])] = Incoming_Data2.apply(lambda x: Layer_Flag_Update_V1(x[''.join([col,"_Replace"])],x[''.join([col,"_ACflag"])] ), axis = 1)
						Incoming_Data2[''.join([col,"_Replace"])] = Incoming_Data2.apply(lambda x: Field_Flag_Chopper(x[''.join([col,"_Replace"])]), axis = 1)
				

				if len(DD_Large_Columns) > 0:
					
					for col in DD_Large_Columns :
						Ref_Data1 = str(DropDownMaster[DropDownMaster.code == col][['displayName','value']].set_index('displayName')['value'].to_dict())
						Incoming_Data2[''.join([col,"_Replace"])] = Incoming_Data2.apply(lambda x: Field_Resolver_Dict_Based(x[''.join([col,"_Replace"])], Ref_Data1 ), axis = 1)
						Incoming_Data2[''.join([col,"_ACflag"])] = Incoming_Data2.apply(lambda x: Layer_Flag_Update_V1(x[''.join([col,"_Replace"])],x[''.join([col,"_ACflag"])] ), axis = 1)
						Incoming_Data2[''.join([col,"_Replace"])] = Incoming_Data2.apply(lambda x: Field_Flag_Chopper(x[''.join([col,"_Replace"])]), axis = 1)
			

                                for col in Incoming_Cols:
					Incoming_Data2[''.join([col,"_ACflag"])][(Incoming_Data2[col] != Incoming_Data2[''.join([col,"_Replace"])]) & (Incoming_Data2[''.join([col,"_ACflag"])] == 'O')]  = 'R'	
							
				return Incoming_Data2
			
			num_cores = multiprocessing.cpu_count()
			Df_Size = len(Incoming_Data3)
			if Df_Size < 25:
				num_partitions = 1
			elif Df_Size < 50:
				num_partitions = 4
			else:
				#num_partitions = math.ceil(Df_Size/float(num_cores)) 
				num_partitions = 10
			
			#number of partitions to split dataframe
			df_split = np.array_split(Incoming_Data3, num_partitions)
			#number of cores on your machine
			pool = ProcessingPool(nodes= num_cores)
			Incoming_Data = pandas.concat(pool.map(DD_MP,df_split))
		
							
                        logging.debug(str(datetime.now() -a ) + 'DD Corrections')
			b = datetime.now()
			logging.debug(str(datetime.now() -a ) + "ML SUCCEED")
			print('ML SUCCEED')
			Incoming_Data = Incoming_Data.replace('',np.nan)		
			return (Incoming_Data[sorted(Incoming_Data.columns)].to_json(orient='records'))
			
	except Exception as e:
		logging.debug('ML_Failed')
		logging.debug(e)
		text_file = open("./JsonLogging/Incoming"+datetime.now().strftime('%Y%m%d%H%M%S%f')+".json", "w")
                text_file.write(Incoming_Json1)
                text_file.close()
		print('ML FAILED')
		return('ML FAILED')
		
			

## Import Fucntions
def DD_Label_Value(DropDown1):
	try:
		DropDownStructure1 = pandas.DataFrame(columns= ['code','DD_Flag1','value','displayName','dataType','questionType'])
		counter = 0
		for  i in DropDown1.options:
			if str(i) == 'nan':
				temp = pandas.DataFrame([[DropDown1.iloc[counter].code,DropDown1.iloc[counter].DD_Status
						,'','',DropDown1.iloc[counter].dataType,DropDown1.iloc[counter].questionType]]
						,columns=['code','DD_Flag1','value','displayName','dataType','questionType'])
				DropDownStructure1 = DropDownStructure1.append(temp)
				counter = counter + 1
			elif (i.keys()[0] == 'items'):
				code = DropDown1.iloc[counter].code
				DD_Status = DropDown1.iloc[counter].DD_Status
				dataType = DropDown1.iloc[counter].dataType
				questionType = DropDown1.iloc[counter].questionType
				for m in i.values()[0]:
					if len(m) > 0:
						temp = pandas.DataFrame([[code,DD_Status,m['value'],m['displayName'],dataType,questionType]]
								,columns=['code','DD_Flag1','value','displayName','dataType','questionType'])
						DropDownStructure1 = DropDownStructure1.append(temp)
				counter = counter + 1
		return DropDownStructure1 
		     
	except Exception as e:
		logging.debug('DD_Label_Value Exception')
		logging.debug(e)
		
		


def Flag_1(code, datatype,questiontype,DropDownMaster):
	try:
		if ((datatype == 'string') & (questiontype == 'text')):
			return 'Freetext'
		elif ((datatype == 'string') & (questiontype == 'dropdown')):
			templist = DropDownMaster[DropDownMaster.code == code]['DD_Flag1'].unique()
			if len(templist) > 0:
				return templist[0]
			return 'false'
		elif ((datatype == 'double') & (questiontype == 'double')):
			return 'double'
		elif ((datatype == 'double') & (questiontype == 'percentage')):
			return 'percentage'
		elif ((datatype == 'int') & (questiontype == 'integer')):
			return 'integer'
		elif ((datatype == 'date') & (questiontype == 'date')):
			return 'date'
	except Exception as e:
		logging.debug('exception Flag_1')
		logging.debug(e)
		return 'False'
	


def Flag_2(Code, Datatype,Questiontype,Flag1, DropDownMaster):
	try:
		if Flag1 not in ('Local'): 
			return 'False'
		DDValueList = sorted(DropDownMaster[DropDownMaster.code == Code]['value'].unique().tolist())
		DDValueListLen = len(DropDownMaster[DropDownMaster.code == Code]['value'].unique().tolist())
		if (('Y' in DDValueList ) & ('N' in DDValueList)):
			return 'DD_Yes_No'
		if (DDValueListLen < 18):
		    return 'DD_Small'
		if (DDValueListLen < 50):
		    return 'DD_Medium'
		return 'DD_Large'
	except Exception as e:
		logging.debug('exception Flag 2')
		logging.debug(e)
		return 'DD_Others'


def DD_Status_Flag(options):
	try:
		if str(options) == 'nondd':
			return 'False'
		if str(options) == 'nan':
			return 'False'
		if str(options).find('To be aligned with MJL') > 0:
			return 'MJL'
		else:
			return 'Local'
	except Exception as e:
		logging.debug('DD_Status_Flag')
		logging.debug(e)
		return 'False'



def is_abbrevation(abbrev, text):
	try:
		abbrev=abbrev.lower()
		text=text.lower()
		words=text.split()
		if not abbrev:
			return True
		if abbrev and not text:
			return False
		if abbrev[0]!=text[0]:
			return False
		else:
			return (is_abbrevation(abbrev[1:],' '.join(words[1:])) or
					any(is_abbrevation(abbrev[1:],text[i+1:])
						for i in range(len(words[0]))))
	except Exception as e:
	        logging.debug('is abbrev')
		logging.debug(e)
		return abbrev


def Field_Resolver_Dict_Based(Field_Replace, DictStr):
	try:
		Dict = eval(DictStr)
		Threshold1 = 40
		Threshold2 = 95
		DD_DisplayNames = Dict.keys()
		DD_Value = Dict.values()
		if Field_Replace is None or Field_Replace == "":
			return(''.join(["%O%",""]))
		Field_Replace = Field_Replace.strip()
		if Field_Replace in DD_Value:
			return(''.join(["%O%",str(Field_Replace)]))
		if Field_Replace in DD_DisplayNames:
			return(''.join(["%R%",Dict[str(Field_Replace)]]))
		if Field_Replace.isdigit():
			return(''.join(["%X%",str(Field_Replace)]))
		DD_String = (' '.join(eval(DictStr).keys()))
		tokens_IncumbentData = Field_Replace.encode('utf-8').split(' ')
		tokens_Ref_Data1 = DD_String.encode('utf-8').split(' ')
		m = 0
		for i in tokens_IncumbentData:
			r1 = filter(lambda x, y = i : is_abbrevation(y, x) ,tokens_Ref_Data1) 
			if len(r1) == 0:
				tokens_IncumbentData[m] = tokens_IncumbentData[m]
			else:
				tokens_IncumbentData[m] = filter(lambda x : len(x) == max(map(lambda x: len(x), r1)) ,r1 )[0]
			m = m +1
		Field_Replace = ' '.join(tokens_IncumbentData)
		DD_Dist = map(lambda x,y = Field_Replace: fuzz.token_sort_ratio(re.sub('[^a-zA-Z0-9 \n\.]', ' ', x).lower(), y.lower()), DD_DisplayNames)
		DD_Dict = dict(zip(DD_Dist, DD_DisplayNames))
		
		if max(DD_Dict.keys()) <= Threshold1:
			return(''.join(["%X%",str(Field_Replace)]))
		elif max(DD_Dict.keys()) <= Threshold2:
			return (''.join(["%R%",Dict[str(DD_Dict.get(max(DD_Dict.keys())))]]))
		else:
			return (''.join(["%R%", Dict[str(DD_Dict.get(max(DD_Dict.keys())))]]))
		
	except Exception as e:
		logging.debug('Field_Resolver')
		logging.debug(e)
		return(''.join(["%X%",str(Field_Replace)]))
		
		


def Field_Flag_Chopper(Replace_String):
	if Replace_String is None:
		return Replace_String
	try:
		if ((Replace_String.startswith("%R%")) | (Replace_String.startswith("%O%")) | (Replace_String.startswith("%X%"))):
			return Replace_String[3:]
		else:
			return Replace_String
	except Exception as e:
                logging.debug("Field Flag chopper")
		logging.debug(e)
		return Replace_String


def Layer_Flag_Update_V1(Replace_String, ACflag):
	if Replace_String is None:
		return "X"
	try:
		if Replace_String.startswith("%R%"):
			return "R"
		elif Replace_String.startswith("%O%"):
			return "O"
		elif Replace_String.startswith("%X%"):
			return "X"
		else:
			return ACflag
	except Exception as e:
		logging.debug("Layer Flag Update")
                logging.debug(e)
		return "X"

def Field_Resolver_Large_DD(Field_Replace, DictStr):
	try:
		Dict = eval(DictStr)
		Threshold1 = 40
		Threshold2 = 95
		DD_DisplayNames = Dict.keys()
		DD_Value = Dict.values()
		if Field_Replace is None or Field_Replace == "":
			return(''.join(["%O%",""]))
                Field_Replace = Field_Replace.strip()
		if Field_Replace in DD_Value:
			return(''.join(["%O%",str(Field_Replace)]))
		if Field_Replace in DD_DisplayNames:
			return(''.join(["%R%",Dict[str(Field_Replace)]]))
		if Field_Replace.isdigit():
			return(''.join(["%X%",str(Field_Replace)]))
		DD_Dist = map(lambda x,y = Field_Replace: fuzz.partial_ratio(y.lower(), x.lower()), DD_DisplayNames)
		DD_Dict = dict(zip(DD_Dist, DD_DisplayNames))
		
		if max(DD_Dict.keys()) <= Threshold1:
			return(''.join(["%X%",str(Field_Replace)]))
		elif max(DD_Dict.keys()) <= Threshold2:
			return (''.join(["%R%",Dict[str(DD_Dict.get(max(DD_Dict.keys())))]]))
		else:
			return (''.join(["%R%", Dict[str(DD_Dict.get(max(DD_Dict.keys())))]]))
		
	except Exception as e:
		logging.debug('Field_Resolver large DD')
		logging.debug(e)
		return(''.join(["%X%",str(Field_Replace)]))
		

def Field_Resolver_Yes_No(Field_Replace, DictStr, flag):
	try:
		Dict = eval(DictStr)
		Threshold1 = 40
		Threshold2 = 95
		DD_DisplayNames = Dict.keys()
		DD_Value = Dict.values()
		if Field_Replace is None or Field_Replace == "":
			return(''.join(["%O%",""]))
		Field_Replace = Field_Replace.strip()
		if flag == 'R':
			return(''.join(["%R%",str(Field_Replace)])) 
		if Field_Replace in DD_Value:
			return(''.join(["%O%",str(Field_Replace)]))
		if Field_Replace in DD_DisplayNames:
			return(''.join(["%R%",Dict[str(Field_Replace)]]))
		if Field_Replace.isdigit():
			return(''.join(["%X%",str(Field_Replace)]))
		DD_Dist = map(lambda x,y = Field_Replace: fuzz.partial_ratio(y.lower(), x.lower()), DD_DisplayNames)
		DD_Dict = dict(zip(DD_Dist, DD_DisplayNames))
		
		if max(DD_Dict.keys()) <= Threshold1:
			return(''.join(["%X%",str(Field_Replace)]))
		elif max(DD_Dict.keys()) <= Threshold2:
			return (''.join(["%R%",Dict[str(DD_Dict.get(max(DD_Dict.keys())))]]))
		else:
			return (''.join(["%R%", Dict[str(DD_Dict.get(max(DD_Dict.keys())))]]))
		
	except Exception as e:
		logging.debug('Yes_No_Field Resolver Exception')
		logging.debug(e)
		return(''.join(["%X%",str(Field_Replace)]))



if __name__ == '__main__':
        logging.debug("In flask")
        app.run(host='0.0.0.0', port=5000,debug=True, use_reloader=False)


