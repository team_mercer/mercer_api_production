package com.lti.mosaic.api.worker.processor;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.lti.mosaic.api.worker.callback.CallBackManager;
import com.lti.mosaic.api.worker.callback.RequestConstants;

import in.lnt.exceptions.CustomStatusException;
import in.lnt.utility.constants.LoggerConstants;
import in.lnt.utility.general.JsonUtils;
import in.lnt.validations.FormValidator;
import in.lti.mosaic.api.base.bso.StatusBSO;
import in.lti.mosaic.api.base.constants.Constants;
import in.lti.mosaic.api.base.constants.Constants.ValidationTypes;
import in.lti.mosaic.api.base.exceptions.SystemException;
import in.lti.mosaic.api.base.serializer.ObjectSerializationHandler;
import in.lti.mosaic.api.base.services.DatastoreServices;
import in.lti.mosaic.api.base.services.EventAPIService;

/**
 * @author rushikesh
 *
 */
public abstract class BaseValidator implements Runnable {

	private static final Logger logger = LoggerFactory.getLogger(L1L2Validator.class);

	private Map<String, String> inputMap;
	private String documentId = null;
	private Integer status = null;
	private String validationType;

	public Map<String, String> getInputMap() {
		return inputMap;
	}

	public void setInputMap(Map<String, String> inputMap) {
		this.inputMap = inputMap;
	}

	public BaseValidator(Map<String, String> map) {
		inputMap = map;
		validationType = map.get(Constants.Request.ValidationType);
	}

	public abstract JsonNode process(Object inputJsonNode) throws Exception;

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {

		logger.info(LoggerConstants.LOG_MAXIQAPI, validationType);

		long documentDownloadedTime = 0;
		long processingTime = 0;
		long documentCreatedTime = 0;
		long startAsyncTime = System.currentTimeMillis();

		String requestId = inputMap.get(Constants.Request.RequestId);
		String documentIdFromRequest = inputMap.get(Constants.Request.DocumentId);
		String envName = inputMap.get(Constants.Request.EnvironmentName);
		
		String callBackJson = null;
		String callBackUrl = null;
		String callBackApikey = null;
		String document = null;
		
		boolean isCallbacked = false;
		boolean isDownloadFailed = false;
		boolean isUploadFailed = false;
		boolean processFail = false;
		try {
			callBackUrl = CallBackManager.getCallBackUrlFromEnvironmentName(envName);
			callBackApikey = CallBackManager.getCallBackApiKeyFromEnvironmentName(envName);
			
			// fetch document from DS using documentId
			try {
				document = DatastoreServices.download(documentIdFromRequest);

				// Dumping request into MongoDB
				FormValidator.dumpToMongo(inputMap.get(Constants.Request.RequestId), document, in.lnt.constants.Constants.REQUEST);

				documentDownloadedTime = System.currentTimeMillis();
				StatusBSO.insertStatusForRequestId(requestId, validationType,
						Constants.RequestStatus.InputDocumentDownloaded, status, envName, documentIdFromRequest, null);
				
			}catch(Exception e) {
				logger.error("Error while performing  {} {}", validationType, e.getMessage());
				isDownloadFailed = true;

				// Mail : Async Workflow changes
				StatusBSO.insertStatusForRequestId(requestId, validationType,
						Constants.RequestStatus.InputDocumentDownloadedError, status, envName, documentIdFromRequest, e.getMessage());
				callBackJson = CallBackManager.createCallBackJson(requestId, documentIdFromRequest, 
						Constants.ResponseStatus.HTTPSTATUS_500, Constants.Request.MOSAIC_DOC_DOWNLOAD_FAILED);				
				try {
					isCallbacked = true;
					CallBackManager.sendResponseViaCallBackUrl(
							callBackJson, callBackUrl, RequestConstants.POST, callBackApikey);
					StatusBSO.insertStatusForRequestId(requestId, validationType, Constants.RequestStatus.DeliveredSuccessFully, status,
							envName, documentId, null);
				}catch(Exception se) {
					logger.error("Error while performing  {} {}", validationType, se.getMessage());
					StatusBSO.insertStatusForRequestId(requestId, validationType, Constants.RequestStatus.DeadLetter, status,
							envName, documentId, se.getMessage());
					throw se;
				}
				throw e;
			}
				


			JsonNode finalOutPut = null;

			try {
				JsonNode outPutOfValidations;

				if (StringUtils.equalsIgnoreCase(validationType, ValidationTypes.L3B)) {
					outPutOfValidations = process(document);

				} else {
					JsonNode entitiesNode = (JsonNode) ObjectSerializationHandler.toObject(document, JsonNode.class);
					outPutOfValidations = process(entitiesNode);
				}
				finalOutPut = outPutOfValidations;
				status = 200;

				processingTime = System.currentTimeMillis();
				StatusBSO.insertStatusForRequestId(requestId, validationType, Constants.RequestStatus.ProcessingFinished,
						status, envName, documentIdFromRequest, null);
				
			} catch (CustomStatusException cse) {
				logger.error("Error while performing  {} {}", validationType, cse.getMessage());
				ObjectNode errNode = new ObjectMapper().createObjectNode();
				errNode = JsonUtils.updateErrorNodeForGenericException(errNode, cse);

				status = cse.getHttpStatus();
				try {
					processFail = true;
					StatusBSO.insertStatusForRequestId(requestId, validationType, Constants.RequestStatus.ProcessingError,
							status, envName, documentIdFromRequest, cse.getMessage());
					
					callBackJson = CallBackManager.createCallBackJson(requestId, documentIdFromRequest, 
							Constants.ResponseStatus.HTTPSTATUS_500, Constants.Request.MOSAIC_PROCESSING_FAILED);				
					isCallbacked = true;
					CallBackManager.sendResponseViaCallBackUrl(
							callBackJson, callBackUrl, RequestConstants.POST, callBackApikey);
					StatusBSO.insertStatusForRequestId(requestId, validationType, Constants.RequestStatus.DeliveredSuccessFully, status,
							envName, documentId, null);
				}catch(Exception se) {
					logger.error("Error while performing  {} {}", validationType, se.getMessage());
					StatusBSO.insertStatusForRequestId(requestId, validationType, Constants.RequestStatus.DeadLetter, status,
							envName, documentId, se.getMessage());
					throw se;
				}
				
			} catch (Exception e) {
				logger.error("Error while performing  {} {}", validationType, e.getMessage());
				ObjectNode errNode = new ObjectMapper().createObjectNode();
				errNode = JsonUtils.updateErrorNodeForGenericException(errNode, e);
				finalOutPut = errNode;
				status = 500;
				try {
					processFail = true;
					StatusBSO.insertStatusForRequestId(requestId, validationType, Constants.RequestStatus.ProcessingError,
							status, envName, documentIdFromRequest, e.getMessage());
					
					callBackJson = CallBackManager.createCallBackJson(requestId, documentIdFromRequest, 
							Constants.ResponseStatus.HTTPSTATUS_500, Constants.Request.MOSAIC_PROCESSING_FAILED);				
					isCallbacked = true;
					CallBackManager.sendResponseViaCallBackUrl(
							callBackJson, callBackUrl, RequestConstants.POST, callBackApikey);
					StatusBSO.insertStatusForRequestId(requestId, validationType, Constants.RequestStatus.DeliveredSuccessFully, status,
							envName, documentId, null);
				}catch(Exception se) {
					logger.error("Error while performing  {} {}", validationType, se.getMessage());
					StatusBSO.insertStatusForRequestId(requestId, validationType, Constants.RequestStatus.DeadLetter, status,
							envName, documentId, se.getMessage());
					throw se;
				}
				
			}

			// Dumping request into MongoDB
			FormValidator.dumpToMongo(inputMap.get(Constants.Request.RequestId),
					ObjectSerializationHandler.toString(finalOutPut), in.lnt.constants.Constants.RESPONSE);
			
			try {
				// Adding event As processing has been finished
				EventAPIService.addEvent(requestId, Constants.RequestStatus.ProcessingFinished);
			} catch (Exception e) {
				logger.error("Error while adding event {}", e.getMessage());
			}

			// Put the output in document Store
			try {
				documentId = DatastoreServices.writeDocs(ObjectSerializationHandler.toString(finalOutPut),
					requestId + envName);

				documentCreatedTime = System.currentTimeMillis();
				StatusBSO.insertStatusForRequestId(requestId, validationType, Constants.RequestStatus.OutputDocumentCreated,
					status, envName, documentId, null);
			}catch(Exception e) {
				logger.error("Error while performing  {} {}", validationType, e.getMessage());
				try {
					isUploadFailed = true;
					StatusBSO.insertStatusForRequestId(requestId, validationType, Constants.RequestStatus.OutputDocumentCreatedError,
							status, envName, documentId, e.getMessage());
					
					callBackJson = CallBackManager.createCallBackJson(requestId, "", 
							Constants.ResponseStatus.HTTPSTATUS_500, Constants.Request.MOSAIC_DOC_UPLOAD_FAILED);				
					isCallbacked = true;
					CallBackManager.sendResponseViaCallBackUrl(
							callBackJson, callBackUrl, RequestConstants.POST, callBackApikey);
					// Mail : Async Workflow changes
					StatusBSO.insertStatusForRequestId(requestId, validationType, Constants.RequestStatus.DeliveredSuccessFully, status,
							envName, documentId, null);
				}catch(Exception se) {
					logger.error("Error while performing  {} {}", validationType, se.getMessage());
					StatusBSO.insertStatusForRequestId(requestId, validationType, Constants.RequestStatus.DeadLetter, status,
							envName, documentId, se.getMessage());
					throw se;
				}
				throw e;
			}


			// send response via CallBackManager Class
			try {
				callBackJson = CallBackManager.createCallBackJson(
						requestId, documentId, status, Constants.Request.MOSAIC_PROCESSING_FINISHED);
				isCallbacked = true;
				CallBackManager.sendResponseViaCallBackUrl(callBackJson, callBackUrl, RequestConstants.POST,
						callBackApikey);

				StatusBSO.insertStatusForRequestId(requestId, validationType,
						Constants.RequestStatus.DeliveredSuccessFully, status, envName, documentId, null);

				// Adding event As processing has been delivered successfully
				try {
					EventAPIService.addEvent(requestId, Constants.RequestStatus.DeliveredSuccessFully);
				} catch (Exception e) {
					logger.error("Error while adding event {}", e.getMessage());
				}

			} catch (Exception e) {

				logger.error("Error while performing  {} {}", validationType, e.getMessage());

				StatusBSO.insertStatusForRequestId(requestId, validationType, Constants.RequestStatus.DeadLetter,
						status, envName, documentId, e.getMessage());

				// Adding event As processing for deadletter
				try {

					EventAPIService.addEvent(requestId, Constants.RequestStatus.DeadLetter);
				} catch (Exception exc) {
					logger.error("Error while performing  {} {}", validationType, exc.getMessage());
				}
			}

		} catch (Exception e) {
			logger.error("Error while performing  {} {}", validationType, e.getMessage());

			// Mail : Async Workflow changes
			if(!isCallbacked) {
				if (null == documentId) {
					documentId = inputMap.get(Constants.Request.DocumentId);
				}
				try {
					callBackJson = CallBackManager.createCallBackJson(
							requestId, documentIdFromRequest, Constants.ResponseStatus.HTTPSTATUS_500, e.getMessage());				
					CallBackManager.sendResponseViaCallBackUrl(
							callBackJson, callBackUrl, RequestConstants.POST, callBackApikey);
					StatusBSO.insertStatusForRequestId(requestId, validationType, Constants.RequestStatus.DeliveredSuccessFully, status,
							envName, documentId, null);
				} catch (Exception e1) {
					logger.error("Error while performing  {} {}", validationType, e1.getMessage());
					StatusBSO.insertStatusForRequestId(requestId, validationType, Constants.RequestStatus.DeadLetter, status,
							envName, documentId, e1.getMessage());
				}
			}
			
		}

		if(isDownloadFailed) {
			logger.error("Info: RequestId is {} Document Download : Failed ");
		} else {
			logger.error("Info: RequestId is {} Document Downloaded Time (Millis): {}  ", requestId,
					(documentDownloadedTime - startAsyncTime));
			if(processFail) {
				logger.error("Info: RequestId is {} Document Process : Failed ");				
			} else {
				logger.error("Info: RequestId is {} API Async Processing Time (Millis): {}  ", requestId,
						(processingTime - documentDownloadedTime));
				if(isUploadFailed) {
					logger.error("Info: RequestId is {} Document Push : Failed ");
				} else {
					logger.error("Info: RequestId is {} Document Pushed Time (Millis): {}  ", requestId,
						(documentCreatedTime - processingTime));
				}
			}
		}		
	}

}