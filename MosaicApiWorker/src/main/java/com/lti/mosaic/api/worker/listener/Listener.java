package com.lti.mosaic.api.worker.listener;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lti.mosaic.api.worker.capacitymanager.CapacityManagerThread;
import com.lti.mosaic.api.worker.message.consumer.MessageConsumerThread;

import in.lnt.utility.general.CacheLoadListener;
import in.lti.mosaic.api.base.message.producer.MessageTypes;

/**
 * This is the worker listener class. Worker execution starts from this class (main method) It reads
 * the pending request based on their priorities and process it.
 * 
 * @author rushi
 * @version 1.0
 * @since 2017-12-26
 */
public class Listener {

  private static final Logger logger = LoggerFactory.getLogger(Listener.class);

  /**
   * Worker execution starts from here
   * 
   * @param args unused
   * @return nothing
   */
  public static void main(String[] args) {
    
    CapacityManagerThread capacityManager = new CapacityManagerThread();
    new Thread(capacityManager).start();
    
    List<String> topics = new ArrayList<String>();

    topics.add(MessageTypes.PERFORM_VALIDATION.toString());

    for (String topic : topics) {
      try {
        MessageConsumerThread messageThread = new MessageConsumerThread(topic);
        logger.info(topic);
        new Thread(messageThread).start();;
      } catch (Throwable e) {
        logger.error("Error in consumer {}", e);
      }
    }
    
    //Derby db load
//    DerbyMainLoader.load();
//    logger.info("Derby is loaded");
    CacheLoadListener.loadCache();
    logger.info("Worker is started");

  }
}
