package com.lti.mosaic.api.worker.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import in.lnt.utility.constants.LoggerConstants;

/**
 * @author rushikesh
 *
 */
public class ValidatorStarter {
  
  private static Logger logger_ = LoggerFactory.getLogger(ValidatorStarter.class);
  
  public static void start(BaseValidator validationBase)
  {
      logger_.debug(LoggerConstants.LOG_MAXIQAPI+" >> start() ");

      new Thread(validationBase).start();
      
      logger_.debug(LoggerConstants.LOG_MAXIQAPI+" << start() ");
  }

}
